package com.example.blb.projecta17;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;//import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import static android.R.attr.fragment;
import static com.example.blb.projecta17.R.id.buttonSignup;
import static com.example.blb.projecta17.R.id.deacti;
import static com.example.blb.projecta17.R.id.textViewSignin;

/**
 * Created by BLB on 06-Mar-17.
 */

public class Settings extends Fragment {
    public static final String TAG = "set";

    private TextView change;
    private TextView edit;
    private TextView deactivate;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.settings, container, false);
        change= (TextView) v.findViewById(R.id.change);
        edit= (TextView) v.findViewById(R.id.edit);
        deactivate= (TextView) v.findViewById(R.id.deacti);

        View.OnClickListener listnr=new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                Fragment fragment;
                if(v == change){
                    //Log.d("BLB", "hai");
                    fragment = getFragmentManager().findFragmentByTag(Change.TAG);
                    if (fragment == null) {
                        fragment = new Change();
                    }
                    getFragmentManager().beginTransaction().replace(R.id.container, fragment, Change.TAG).commit();
                }
                if (v==edit)
                {
                    //Log.d("BLB", "hai BLB");
                    fragment = getFragmentManager().findFragmentByTag(Edit.TAG);
                    if (fragment == null) {
                        fragment = new Edit();
                    }
                    getFragmentManager().beginTransaction().replace(R.id.container, fragment, Edit.TAG).commit();
                }
                if (v==deactivate){
                    fragment = getFragmentManager().findFragmentByTag(Deactivate.TAG);
                    if (fragment == null) {
                        fragment = new Deactivate();
                    }
                    getFragmentManager().beginTransaction().replace(R.id.container, fragment, Deactivate.TAG).commit();
                }
            }
        };
        change.setOnClickListener(listnr);
        edit.setOnClickListener(listnr);
        deactivate.setOnClickListener(listnr);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
  /*  public void onClick(View view) {
        if (view == change) {
           // registerUser();
        }

        if (view == edit) {
            //open login activity when user taps on the already registered textview
            //startActivity(new Intent(this, LoginActivity.class));

        }
    }*/
}
