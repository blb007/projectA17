package com.example.blb.projecta17;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import static android.R.attr.fragment;
import static android.R.attr.name;
import static com.example.blb.projecta17.R.array.chestpain;
import static com.example.blb.projecta17.R.id.age;
import static com.example.blb.projecta17.R.id.bmi;
import static com.example.blb.projecta17.R.id.bt;
import static com.example.blb.projecta17.R.id.dia;
import static com.example.blb.projecta17.R.id.glu;
import static com.example.blb.projecta17.R.id.hrd;
import static com.example.blb.projecta17.R.id.hrm;
import static com.example.blb.projecta17.R.id.rbp;
import static com.example.blb.projecta17.R.id.roomT;
import static com.example.blb.projecta17.R.id.roomh;
import static com.example.blb.projecta17.R.id.uage;
import static com.example.blb.projecta17.R.id.ubmi;
import static com.example.blb.projecta17.R.id.ubp;
import static com.example.blb.projecta17.R.id.uches;
import static com.example.blb.projecta17.R.id.udia;
import static com.example.blb.projecta17.R.id.ugen;
import static com.example.blb.projecta17.R.id.uglu;
import static com.example.blb.projecta17.R.id.uhd;
import static com.example.blb.projecta17.R.id.uhr;
import static com.example.blb.projecta17.R.id.uname;
import static com.example.blb.projecta17.R.id.uphone;
import static com.example.blb.projecta17.R.id.urbp;
import static com.example.blb.projecta17.R.id.ure;

/**
 * Created by BLB on 06-Mar-17.
 */

public class TabHome extends Fragment {
    TextView hrate;
    TextView diab;
    TextView hrtdis;
    TextView bta;
    TextView rt;
    TextView rh;
    private ValueEventListener userInfoListener;
    private FirebaseAuth firebaseAuth;
    private FirebaseUser user;
    private FirebaseDatabase database;
    private DatabaseReference ref;
    private Button buttonSuggest;

    //Overriden method onCreateView
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.tab_home, container, false);

        firebaseAuth= FirebaseAuth.getInstance();
        user = firebaseAuth.getCurrentUser();
        database = FirebaseDatabase.getInstance();
        ref = FirebaseDatabase.getInstance().getReference(user.getUid());
        // getting views
        hrate=(TextView) v.findViewById(hrm);
        diab=(TextView)v.findViewById(dia);
        hrtdis=(TextView)v.findViewById(hrd);
        bta=(TextView)v.findViewById(bt);
        rt=(TextView)v.findViewById(roomT);
        rh=(TextView)v.findViewById(roomh);

        buttonSuggest=(Button) v.findViewById(R.id.suggest);
        View.OnClickListener listnr=new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                if(v == buttonSuggest){
                    process();
                }
            }
        };
        buttonSuggest.setOnClickListener(listnr);

        listen();
        return v;
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    private void listen() {
        ref = FirebaseDatabase.getInstance().getReference(user.getUid());
        userInfoListener =  ref.addValueEventListener(new ValueEventListener() {
            public void onDataChange(DataSnapshot dataSnapshot) {
                // You can use getValue to deserialize the data at dataSnapshot
                User post = dataSnapshot.getValue(User.class);
                String mh=post.getMaxheartrate().toString();
                hrate.setText(mh);
                String diabe;
                if(post.getDiabetes()==0) {
                    diabe = "No";
                    diab.setTextColor(Color.BLACK);
                }
                else {diabe="Yes";
                    diab.setTextColor(Color.RED);}
                diab.setText(diabe);

                String hdi;
                if (post.getHeartdisease()==0)
                {
                    hdi = "No";
                    hrtdis.setTextColor(Color.BLACK);
                }
                else {hdi="Yes";hrtdis.setTextColor(Color.RED);}
                hrtdis.setText(hdi);

            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
    private void process() {
      /*  Fragment fragment = getFragmentManager().findFragmentByTag(Suggest.TAG);
        if (fragment == null) {
            fragment = new Suggest();
        }
        getFragmentManager().beginTransaction().replace(R.id.container, fragment, Suggest.TAG).commit();*/
        LayoutInflater li = LayoutInflater.from(getContext());
        final View promptsView = li.inflate(R.layout.prompt_diet, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);
        alertDialogBuilder.setCancelable(false).setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {

                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        final TextView userDiet = (TextView) promptsView.findViewById(R.id.dietText);

        ref.addValueEventListener(new ValueEventListener() {
            public void onDataChange(DataSnapshot dataSnapshot) {
                // You can use getValue to deserialize the data at dataSnapshot
                User post = dataSnapshot.getValue(User.class);
                int flag=0;
                if(post.getDiabetes()==1)
                {   flag++;
                    userDiet.setText("1.Take Proteins(g)=49.5\n2.Calories(kcal)=1286 per day" +
                            "\n3.Avoid Table sugar, honey, molasses and syrup, Regular soft drinks, fruit drinks,Canned fruits with heavy syrup");
                }
                if(post.getHeartdisease()==1)
                {   flag++;
                    userDiet.setText("1.Control your portion size\n2.Eat more vegetables and fruits\n3.Select whole grains\n4.Limit unhealthy fats\n5.Choose low-fat protein sources\n6.Reduce the sodium in your food\n7.Plan ahead: Create daily menus\n8.Allow yourself an occasional treat");
                }
                if(flag==2)
                {
                    userDiet.setText("1.Lose weight if you are overweight\n2.Regular physical activity\n3.Have a low salt intake\n4.Eat a healthy diet\n5.Drink alcohol in moderation");
                }
              /*  if(post.getBp()<120)
                {
                    userDiet.setText("Take salt content fooods more");
                }
                if(post.getBp()>140)
                {
                    userDiet.setText("Avoid salt in your diet");
                }*/
               else if(post.getDiabetes()==0&&post.getHeartdisease()==0)
                {
                    if(post.getBp()<120)
                    {
                        userDiet.setText("Low BP:-Take salt content foods more");
                    }
                    else if(post.getBp()>140)
                    {
                        userDiet.setText("High BP:-Avoid salt in your diet");
                    }
                    else userDiet.setText("Nothing to control\nEnjoy life with care");
                }

            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        // show it
        alertDialog.show();
    }
}
