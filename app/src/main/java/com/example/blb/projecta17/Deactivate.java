package com.example.blb.projecta17;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import static android.R.attr.button;
import static com.example.blb.projecta17.R.id.buttonLogout;

/**
 * Created by BLB on 12-Mar-17.
 */

public class Deactivate extends Fragment {
    public static final String TAG = "deacti";

   /* private FirebaseAuth firebaseAuth;
    private FirebaseUser user;
    private FirebaseDatabase database;
    private DatabaseReference ref;*/
  private String password;
    private Button buttonAccept;
    private Button buttonCancel;
    private ProgressDialog progressDialog;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.deactivate, container, false);
        /*firebaseAuth = FirebaseAuth.getInstance();
        user = firebaseAuth.getCurrentUser();
        database = FirebaseDatabase.getInstance();
        ref = FirebaseDatabase.getInstance().getReference(user.getUid());*/
        //getting current user
        //FirebaseUser user = firebaseAuth.getCurrentUser();
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        View.OnClickListener listnr=new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                if(v == buttonAccept){
                    // Get auth credentials from the user for re-authentication. The example below shows
                    // email and password credentials but there are multiple possible providers,
                    // such as GoogleAuthProvider or FacebookAuthProvider.
                    progressDialog = new ProgressDialog(getActivity());
                    LayoutInflater li = LayoutInflater.from(getContext());
                    View promptsView = li.inflate(R.layout.prompt, null);
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
                    // set prompts.xml to alertdialog builder
                    alertDialogBuilder.setView(promptsView);
                    final EditText userInput = (EditText) promptsView.findViewById(R.id.promptUserInput);
                    alertDialogBuilder.setCancelable(false).setPositiveButton("OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,int id) {
                                            // get user input and set it to result
                                            // edit text
                                            password =userInput.getText().toString();
                                            Log.d(TAG, password);
                                            AuthCredential credential = EmailAuthProvider.getCredential(user.getEmail(),password);
                                            progressDialog.setMessage("Checking password Please Wait...");
                                            progressDialog.show();
                                            user.reauthenticate(credential).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    progressDialog.setMessage("Deleting user data..");
                                                    user.delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                                                        @Override
                                                        public void onComplete(@NonNull Task<Void> task) {
                                                            if (task.isSuccessful()) {
                                                                progressDialog.setMessage("finishing..");
                                                                progressDialog.dismiss();
                                                                Toast.makeText(getActivity(), "User account deleted", Toast.LENGTH_LONG).show();
                                                                startActivity(new Intent(getContext(), LoginActivity.class));
                                                                //ref.removeValue();
                                                            }
                                                            else
                                                            {
                                                                progressDialog.setMessage("Error occurred..");
                                                                progressDialog.dismiss();
                                                                Toast.makeText(getActivity(), "Please try again later", Toast.LENGTH_LONG).show();
                                                            }
                                                        }
                                                    });

                                                }
                                            });
                                        }
                                    })
                            .setNegativeButton("Cancel",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,int id) {
                                            dialog.cancel();
                                        }
                                    });
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    // show it
                    alertDialog.show();

                }
                if (v==buttonCancel)
                {

                    Fragment fragment = getFragmentManager().findFragmentByTag(Dashboard.TAG);
                    if (fragment == null) {
                        fragment = new Dashboard();
                    }
                    getFragmentManager().beginTransaction().replace(R.id.container, fragment, Dashboard.TAG).commit();
                }
            }
        };
        buttonAccept = (Button) v.findViewById(R.id.buttonProceed);
        buttonCancel=(Button) v.findViewById(R.id.buttonCancel);
        buttonAccept.setOnClickListener(listnr);
        buttonCancel.setOnClickListener(listnr);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

}
