package com.example.blb.projecta17;

public interface NavigationDrawerCallbacks {
    void onNavigationDrawerItemSelected(int position);
}
