package com.example.blb.projecta17;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
/*import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.dataproc.Dataproc;
import com.google.api.services.dataproc.model.Job;
import com.google.api.services.dataproc.model.JobPlacement;
import com.google.api.services.dataproc.model.JobReference;
import com.google.api.services.dataproc.model.SparkJob;
import com.google.api.services.dataproc.model.SubmitJobRequest;
import com.google.common.collect.ImmutableList;*/
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.Collections;
import java.util.UUID;
import java.util.logging.Logger;
import static android.R.attr.data;
import static android.R.attr.process;
import static android.support.v7.widget.AppCompatDrawableManager.get;
import static com.example.blb.projecta17.R.id.Password;
import static com.example.blb.projecta17.R.id.age;
import static com.example.blb.projecta17.R.id.buttonCancel;
import static com.example.blb.projecta17.R.id.buttonLogout;
import static com.example.blb.projecta17.R.id.glu;

/**
 * Created by BLB on 06-Mar-17.
 */

public class TabDiabetes extends Fragment {
    private ValueEventListener userInfoListener;
    private FirebaseAuth firebaseAuth;
    private FirebaseUser user;
    private FirebaseDatabase database;
    private DatabaseReference ref;

    private EditText Uage;
    private EditText Ubmi;
    private EditText Uglu;
    private Button Usubmit;

    Logger logger = null;
    User post;
    ViewPager viewPager;
    //Overriden method onCreateView
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.tab_diabetes, container, false);

        Uage = (EditText) v.findViewById(R.id.age);
        Ubmi = (EditText) v.findViewById(R.id.bmi);
        Uglu = (EditText) v.findViewById(glu);
        Usubmit = (Button) v.findViewById(R.id.submit);

        viewPager = (ViewPager) getActivity().findViewById(R.id.viewpager);
        firebaseAuth= FirebaseAuth.getInstance();
        user = firebaseAuth.getCurrentUser();
        database = FirebaseDatabase.getInstance();
        ref = FirebaseDatabase.getInstance().getReference(user.getUid());
        userInfoListener =  ref.addValueEventListener(new ValueEventListener() {
            public void onDataChange(DataSnapshot dataSnapshot) {
                post = dataSnapshot.getValue(User.class);
                Uage.setText(post.getAge().toString(),TextView.BufferType.EDITABLE);
                Uglu.setText(post.getGlucose().toString(),TextView.BufferType.EDITABLE);
                Ubmi.setText(post.getBmi().toString(),TextView.BufferType.EDITABLE);
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        View.OnClickListener listnr=new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                if(v == Usubmit){
                    final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                    try {
                        process();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }
        };
        Usubmit.setOnClickListener(listnr);
        return v;
    }

    private void process() throws Exception {
        firebaseAuth= FirebaseAuth.getInstance();
        user = firebaseAuth.getCurrentUser();
        database = FirebaseDatabase.getInstance();
        ref = FirebaseDatabase.getInstance().getReference(user.getUid());

         int age1 = Integer.parseInt( Uage.getText().toString() );
       // int bmi1 = Integer.parseInt( Ubmi.getText().toString() );
        int glu1 = Integer.parseInt( Uglu.getText().toString() );
        double bmi1 = Double.parseDouble(Ubmi.getText().toString());
        post.setFcm(FirebaseInstanceId.getInstance().getToken());
        post.setDiabetes(checkKNN());
        post.setAge(age1);
        post.setBmi(bmi1);
        post.setGlucose(glu1);
        ref.setValue(post);
        Toast.makeText(getActivity(), "updated", Toast.LENGTH_LONG).show();
        viewPager.setCurrentItem(0);
    }
    public int checkKNN(){
        int n=500;
        double q1,q3,q2,temp;

        String In[] = new String[n];
        int d[] = new int[n];
        int c[] = new int[n];
        int r[] = new int[n];

        double x1[] ={109,158,88,92,122,103,138,102,90,111,180,133,106,171,159,180,146,71,103,105,103,101,88,176,150,73,187,100,146,105,84,133,44,141,114,99,109,109,95,146,100,139,126,129,79,0,62,95,131,112,113,74,83,101,137,110,106,100,136,107,80,123,81,134,142,144,92,71,93,122,163,151,125,81,85,126,96,144,83,95,171,155,89,76,160,146,124,78,97,99,162,111,107,132,113,88,120,118,117,105,173,122,170,84,96,125,100,93,129,105,128,106,108,108,154,102,57,106,147,90,136,114,156,153,188,152,99,109,88,163,151,102,114,100,131,104,148,120,110,111,102,134,87,79,75,179,85,129,143,130,87,119,0,73,141,194,181,128,109,139,111,123,159,135,85,158,105,107,109,148,113,138,108,99,103,111,196,162,96,184,81,147,179,140,112,151,109,125,85,112,177,158,119,142,100,87,101,162,197,117};//{148,85,183,89,137,116,78,115,197,125};
        double x2[] ={36,31.6,24.8,19.9,27.6,24,33.2,32.9,38.2,37.1,34,40.2,22.7,45.4,27.4,42,29.7,28,39.1,0,19.4,24.2,24.4,33.7,34.7,23,37.7,46.8,40.5,41.5,0,32.9,25,25.4,32.8,29,32.5,42.7,19.6,28.9,32.9,28.6,43.4,35.1,32,24.7,32.6,37.7,43.2,25,22.4,0,29.3,24.6,48.8,32.4,36.6,38.5,37.1,26.5,19.1,32,46.7,23.8,24.7,33.9,31.6,20.4,28.7,49.7,39,26.1,22.5,26.6,39.6,28.7,22.4,29.5,34.3,37.4,33.3,34,31.2,34,30.5,31.2,34,33.7,28.2,23.2,53.2,34.2,33.6,26.8,33.3,55,42.9,33.3,34.5,27.9,29.7,33.3,34.5,38.3,21.1,33.8,30.8,28.7,31.2,36.9,21.1,39.5,32.5,32.4,32.8,0,32.8,30.5,33.7,27.3,37.4,21.9,34.3,40.6,47.9,50,24.6,25.2,29,40.9,29.7,37.2,44.2,29.7,31.6,29.9,32.5,29.6,31.9,28.4,30.8,35.4,28.9,43.5,29.7,32.7,31.2,67.1,45,39.1,23.2,34.9,27.7,26.8,27.6,35.9,30.1,32,27.9,31.6,22.6,33.1,30.4,52.3,24.4,39.4,24.3,22.9,34.8,30.9,31,40.1,27.3,20.4,37.7,23.9,37.5,37.7,33.2,35.5,27.7,42.8,34.2,42.6,34.2,41.8,35.8,30,29,37.8,34.6,31.6,25.2,28.8,23.6,34.6,35.7,37.2,36.7,45.2};//{33.6,22.6,22.3,28.1,43.1,25.6,31,35.3,30.5,22.6};
        double x3[]={60,28,22,28,45,33,35,46,27,56,26,37,48,54,40,25,29,22,31,24,22,26,30,58,42,21,41,31,44,22,21,39,36,24,42,32,38,54,25,27,28,26,42,23,22,22,41,27,26,24,22,22,36,22,37,27,45,26,43,24,21,34,42,60,21,40,24,22,23,31,33,22,21,24,27,21,27,37,25,24,24,46,23,25,39,61,38,25,22,21,25,24,23,69,23,26,30,23,40,62,33,33,30,39,26,31,21,22,29,28,55,38,22,42,23,21,41,34,65,22,24,37,42,23,43,36,21,23,22,47,36,45,27,21,32,41,22,34,29,29,36,29,25,23,33,36,42,26,47,37,32,23,21,27,40,41,60,33,31,25,21,40,36,40,42,29,21,23,26,29,21,28,32,27,55,27,57,52,21,41,25,24,60,24,36,38,25,32,32,41,21,66,37,61,26,22,26,24,31,24};//{50,31,32,21,33,30,26,29,53,54};
        String clasi[] = {"good","bad","good","good","good","good","good","bad","bad","bad","good","good","good","bad","good","bad","good","good","bad","good","good","good","good","bad","good","good","bad","good","good","good","good","bad","good","good","bad","good","bad","good","good","good","bad","good","bad","good","good","good","good","good","bad","good","good","good","good","good","bad","good","good","good","bad","good","good","good","good","bad","good","good","good","good","good","bad","bad","good","good","good","good","good","good","good","good","bad","bad","bad","good","good","bad","bad","bad","good","good","good","bad","good","good","good","bad","bad","good","good","bad","bad","bad","bad","bad","good","good","good","good","good","good","good","good","good","good","bad","good","good","good","good","good","good","good","good","bad","good","bad","bad","good","good","good","bad","good","good","good","good","bad","bad","good","good","good","good","bad","bad","good","good","good","bad","good","bad","good","bad","good","good","good","good","good","bad","bad","bad","bad","bad","good","good","bad","bad","good","bad","good","bad","bad","bad","good","good","good","good","good","good","bad","bad","good","bad","good","good","good","bad","bad","bad","bad","good","bad","bad","bad","bad","good","good","good","good","good","bad","good","good"};//{"bad","good","bad","good","bad","good","bad","good","bad","bad"};

        q1=Integer.parseInt( Uglu.getText().toString() );
        q2=Double.parseDouble(Ubmi.getText().toString());
        q3=Integer.parseInt( Uage.getText().toString() );

        int k=3;

        for(int i=0;i<n;i++)
        {
            temp=(int)((x1[i]-q1)*(x1[i]-q1))+((x2[i]-q2)*(x2[i]-q2))+((x3[i]-q3)*(x3[i]-q3));
            d[i]=(int)temp;
        }
        for(int i=0;i<n;i++)
        {
            c[i]=d[i];
        }

        for(int i=0;i<n;i++)
        {
            for(int j=1;j<(n-i);j++)
             {
                if(d[j-1]>d[j])
                 {
                     int t = d[j-1];
                     d[j-1] = d[j];
                     d[j] = t;
                }
             }
        }

        for(int i=0;i<n;i++)
        {
            for(int j=0;j<n;j++)
            {
                if(c[i]==d[j])
                 {
                     r[i] = j+1;
                     continue;
                 }
            }
        }
        int g=0,b=0;
        for(int i=0;i<n;i++)
        {
            if(r[i] < (k+1))
            {
                In[i]="yes";
                if(clasi[i].equals("good"))
                    g++;
                 else
                     b++;
            }
            else
                In[i]="no";
        }
        if(g > b)
            return 0;
        else
            return 1;
    }
}