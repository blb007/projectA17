package com.example.blb.projecta17;

/**
 * Created by BLB on 04-Mar-17.
 */

public class User {
    public static String name;
    public Integer age;
    public String gender;
    public String phone;
    public Double bmi;
    public Integer bp;
    public Integer glucose;
    public Integer diabetes;
    public Integer chestpain;
    public Integer restbp;
    public Integer exercise;
    public Integer restelectro;
    public Integer maxheartrate;
    public Integer heartdisease;
    public String fcm;
    public Integer hrt;
    public String doc;

    public User(){/* Default constructor required for calls to DataSnapshot.getValue(User.class)*/}
    public User(String name,Integer age,String gender,String phone)
    {

        this.name = name;
        this.age=age;
        this.gender=gender;
        this.phone=phone;
        this.bmi=0.0;
        this.bp=0;
        this.glucose=0;
        this.diabetes=0;
        this.chestpain=0;
        this.restbp=0;
        this.exercise=0;
        this.restelectro=0;
        this.maxheartrate=0;
        this.heartdisease=0;
        this.fcm="";
        this.hrt=0;
    }
    public User(String name,Integer age,String gender,String phone,Double bmi,Integer bp,Integer glucose,Integer diabetes,
                Integer chestpain,Integer restbp,Integer exercise,Integer restelectro,Integer maxheartrate,Integer heartdisease)
    {

        this.name = name;
        this.age = age;
        this.gender=gender;
        this.phone=phone;
        this.bmi=bmi;
        this.bp=bp;
        this.glucose=glucose;
        this.diabetes=diabetes;
        this.chestpain=chestpain;
        this.restbp=restbp;
        this.exercise=exercise;
        this.restelectro=restelectro;
        this.maxheartrate=maxheartrate;
        this.heartdisease=heartdisease;

    }
    public String getName() {
        return name;
    }
    public Integer getAge() {
        return age;
    }
    public String getGender() {
        return gender;
    }
    public String getPhone() {
        return phone;
    }
    public Double getBmi() {
        return bmi;
    }
    public Integer getBp() {
        return bp;
    }
    public Integer getGlucose() {
        return glucose;
    }
    public Integer getDiabetes() {
        return diabetes;
    }
    public Integer getChestpain() {
        return chestpain;
    }
    public Integer getRestbp() {
        return restbp;
    }
    public Integer getRestelectro() {
        return restelectro;
    }
    public Integer getMaxheartrate() {
        return maxheartrate;
    }
    public Integer getHeartdisease() {
        return heartdisease;
    }
    public Integer getExercise(){return exercise;}
    public String getFcm(){return fcm;}
    public Integer getHrt(){return hrt;}
    public String getDoc(){return doc;}

    //setting data
    public void setFcm(String fcm){this.fcm=fcm;}
    public void setAge(Integer age) {
        this.age = age;
    }
    public void setGlucose(Integer glu){
        this.glucose=glu;
    }
    public void setBmi(Double bmi1){this.bmi=bmi1;}
    public void setBp(Integer bp){this.bp=bp;}
    public void setDiabetes(Integer dia){this.diabetes=dia;}
    public void setMaxheartrate(Integer max){this.maxheartrate=max;}
    public void setRestelectro(Integer rst){this.restelectro=rst; }
    public void setHeartdisease(Integer hd){this.heartdisease=hd;}
    public void setRestbp(Integer rbp){this.restbp=rbp;}
    public void setChestpain(Integer cp){this.chestpain=cp;}
    public void setExercise(Integer ex){this.exercise=ex;}
    public void setName(String nm){this.name=nm;}
    public void setGender(String g){this.gender=g;}
    public void setPhone(String ph){this.phone=ph;}
    public void setHrt(Integer ht){this.hrt=ht;}
}
