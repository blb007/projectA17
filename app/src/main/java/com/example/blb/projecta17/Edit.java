package com.example.blb.projecta17;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import static android.R.attr.fragment;
import static com.example.blb.projecta17.Dashboard.viewPager;
import static com.example.blb.projecta17.R.id.glu;
import static com.example.blb.projecta17.R.id.radioGender;

/**
 * Created by BLB on 12-Mar-17.
 */

public class Edit extends Fragment {
    public static final String TAG = "edit";
    private ProgressDialog progressDialog;
    private EditText name;
    private EditText age;
    private EditText phone;
    private EditText bp;
    private Button buttonSave;

    private RadioButton radioBtn;
    private RadioGroup radioGrp;

    private ValueEventListener userInfoListener;
    private FirebaseAuth firebaseAuth;
    private FirebaseUser user;
    private FirebaseDatabase database;
    private DatabaseReference ref;

    User post;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.edit, container, false);
        progressDialog = new ProgressDialog(getActivity());
        name = (EditText) v.findViewById(R.id.editname);
        age = (EditText) v.findViewById(R.id.editage);
        phone = (EditText) v.findViewById(R.id.editphone);
        bp=(EditText)v.findViewById(R.id.editBP);
        radioGrp= (RadioGroup) v.findViewById(R.id.radioGender);
        buttonSave = (Button) v.findViewById(R.id.buttonSave);
        int selectedId = radioGrp.getCheckedRadioButtonId();
        radioBtn=(RadioButton)v.findViewById(selectedId);

        firebaseAuth= FirebaseAuth.getInstance();
        user = firebaseAuth.getCurrentUser();
        database = FirebaseDatabase.getInstance();
        ref = FirebaseDatabase.getInstance().getReference(user.getUid());
        userInfoListener =  ref.addValueEventListener(new ValueEventListener() {
            public void onDataChange(DataSnapshot dataSnapshot) {
                post = dataSnapshot.getValue(User.class);
                bp.setText(post.getBp().toString());
                age.setText(post.getAge().toString());
               radioGrp.check(post.getGender().equals("Male")? R.id.radioMale : R.id.radioFemale);
                name.setText(post.getName());
                phone.setText(post.getPhone());
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        View.OnClickListener listnr=new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                if(v == buttonSave){
                    final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                    process();
                }
            }
        };
        buttonSave.setOnClickListener(listnr);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void process() {
        String name1=name.getText().toString();
        int age1=Integer.parseInt(String.valueOf(age.getText()));
        String gender1=radioBtn.getText().toString().trim();
        String phone1=phone.getText().toString();
        int bp1=Integer.parseInt(String.valueOf(bp.getText()));
        post.setAge(age1);
        post.setName(name1);
        post.setGender(gender1);
        post.setPhone(phone1);
        post.setBp(bp1);
        post.setFcm(FirebaseInstanceId.getInstance().getToken());
        ref.setValue(post);
        Fragment fragment = getFragmentManager().findFragmentByTag(HealthRec.TAG);
        if (fragment == null) {
            fragment = new HealthRec();
        }
        getFragmentManager().beginTransaction().replace(R.id.container, fragment, HealthRec.TAG).commit();

    }
}
