package com.example.blb.projecta17;

import android.app.Activity;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;//import android.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import static android.R.attr.fragment;
import static com.example.blb.projecta17.R.id.uage;
import static com.example.blb.projecta17.R.id.ubmi;
import static com.example.blb.projecta17.R.id.ubp;
import static com.example.blb.projecta17.R.id.udia;
import static com.example.blb.projecta17.R.id.ugen;
import static com.example.blb.projecta17.R.id.uglu;
import static com.example.blb.projecta17.R.id.uname;
import static com.example.blb.projecta17.R.id.uphone;


public class Main2Activity extends ActionBarActivity
        implements NavigationDrawerCallbacks {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;
    private Toolbar mToolbar;
    String name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(mToolbar);


        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.fragment_drawer);

        // Set up the drawer.
        mNavigationDrawerFragment.setup(R.id.fragment_drawer, (DrawerLayout) findViewById(R.id.drawer), mToolbar);
        // populate the navigation drawer
        FirebaseAuth firebaseAuth= FirebaseAuth.getInstance();
        //getting current user
        if(firebaseAuth.getCurrentUser() == null){
            //close this activity
            finish();
            //opening profile activity
            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
        }
        final FirebaseUser user = firebaseAuth.getCurrentUser();
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference(user.getUid());

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User post = dataSnapshot.getValue(User.class);
                name=post.getName();
                //Toast.makeText(Main2Activity.this, name, Toast.LENGTH_LONG).show();
                mNavigationDrawerFragment.setUserData(name, user.getEmail(), BitmapFactory.decodeResource(getResources(), R.drawable.avatar));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });

    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
       //Toast.makeText(this, "Menu item selected -> " + position, Toast.LENGTH_SHORT).show();
       Fragment fragment;
        switch (position) {
            case 0: fragment = getSupportFragmentManager().findFragmentByTag(Dashboard.TAG);
                if (fragment == null) {
                    fragment = new Dashboard();
                }
                getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment, Dashboard.TAG).commit();
                break;
            case 1: //stats
                fragment = getSupportFragmentManager().findFragmentByTag(HealthRec.TAG);
                if (fragment == null) {
                    fragment = new HealthRec();
                }
                getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment, HealthRec.TAG).commit();
                break;
            case 2: fragment = getSupportFragmentManager().findFragmentByTag(Settings.TAG);
                if (fragment == null) {
                    fragment = new Settings();
                }
                getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment, Settings.TAG).commit();
                break;
            case 3:fragment = getSupportFragmentManager().findFragmentByTag(Contact.TAG);
                if (fragment == null) {
                    fragment = new Contact();
                }
                getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment, Contact.TAG).commit();
                break;
            case 4:fragment = getSupportFragmentManager().findFragmentByTag(About.TAG);
                if (fragment == null) {
                    fragment = new About();
                }
                getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment, About.TAG).commit();
                break;
            case 5:fragment = getSupportFragmentManager().findFragmentByTag(Logout.TAG);
                if (fragment == null) {
                    fragment = new Logout();
                }
                getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment, Logout.TAG).commit();
                break;
        }
    }


    @Override
    public void onBackPressed() {
        if (mNavigationDrawerFragment.isDrawerOpen())
            mNavigationDrawerFragment.closeDrawer();
        else
            super.onBackPressed();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main2, menu);
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Fragment fragment = getSupportFragmentManager().findFragmentByTag(Settings.TAG);
            if (fragment == null) {
                fragment = new Settings();
            }
            getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment, Settings.TAG).commit();
          //  return true;
        }
        if(id==R.id.action_home)
        {
            Fragment fragment = getSupportFragmentManager().findFragmentByTag(Dashboard.TAG);
            if (fragment == null) {
                fragment = new Dashboard();
            }
            getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment, Dashboard.TAG).commit();
        }
        if(id==R.id.action_app)
        {
            Fragment fragment = getSupportFragmentManager().findFragmentByTag(App.TAG);
            if (fragment == null) {
                fragment = new App();
            }
            getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment, App.TAG).commit();
        }
        if(id==R.id.action_msg)
        {
            Fragment fragment = getSupportFragmentManager().findFragmentByTag(Chat.TAG);
            if (fragment == null) {
                fragment = new Chat();
            }
            getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment, Chat.TAG).commit();
        }

        return super.onOptionsItemSelected(item);
    }


}
