package com.example.blb.projecta17;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import static android.R.attr.max;
import static android.R.attr.y;
import static com.example.blb.projecta17.R.id.age;
import static com.example.blb.projecta17.R.id.radioexYes;

/**
 * Created by BLB on 06-Mar-17.
 */

public class TabHeart extends Fragment {

    private Spinner Ucp, Urew;
    private EditText Uage;
    private EditText Urbp;
    private EditText Umx;
    private RadioGroup Ubs,Uex;
    private Button Usubmit;
    private RadioButton radioUbs,radioEx;

    private ValueEventListener userInfoListener;
    private FirebaseAuth firebaseAuth;
    private FirebaseUser user;
    private FirebaseDatabase database;
    private DatabaseReference ref;

    User post;
    ViewPager viewPager;
    //Overriden method onCreateView
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.tab_heart, container, false);
        //Returning the layout file after inflating
        //Change R.layout.tab1 in you classes
        Ucp = (Spinner) v.findViewById(R.id.spinner1);
        Urew = (Spinner) v.findViewById(R.id.spinner2);
        //Spinners
        ArrayAdapter<CharSequence> staticAdapter = ArrayAdapter
                .createFromResource(this.getActivity(), R.array.chestpain,
                        android.R.layout.simple_spinner_item);
        staticAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Ucp.setAdapter(staticAdapter);
        //Ucp.setAdapter(staticAdapter);

        Ucp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
              //  Log.d("item", (String) parent.getItemAtPosition(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });
        ArrayAdapter<CharSequence> Adapter = ArrayAdapter
                .createFromResource(this.getActivity(), R.array.restElectro,
                        android.R.layout.simple_spinner_item);
        Adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Urew.setAdapter(Adapter);
        Urew.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
               // Log.d("item", (String) parent.getItemAtPosition(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });


        Uage=(EditText)v.findViewById(age);
        Urbp=(EditText)v.findViewById(R.id.rbp);
        Umx=(EditText)v.findViewById(R.id.mx);
        Ubs = (RadioGroup) v.findViewById(R.id.radioBS);
        Uex=(RadioGroup)v.findViewById(R.id.radioex);
        // get selected radio button from radioGroup
        int selectedId = Ubs.getCheckedRadioButtonId();
        int seletedid=Uex.getCheckedRadioButtonId();
        // find the radiobutton by returned id
        radioUbs = (RadioButton) v.findViewById(selectedId);
        radioEx=(RadioButton)v.findViewById(seletedid);
        Usubmit = (Button) v.findViewById(R.id.submit);

        viewPager = (ViewPager) getActivity().findViewById(R.id.viewpager);

        firebaseAuth= FirebaseAuth.getInstance();
        user = firebaseAuth.getCurrentUser();
        database = FirebaseDatabase.getInstance();
        ref = FirebaseDatabase.getInstance().getReference(user.getUid());
        userInfoListener =  ref.addValueEventListener(new ValueEventListener() {
            public void onDataChange(DataSnapshot dataSnapshot) {
                post = dataSnapshot.getValue(User.class);
                Uage.setText(post.getAge().toString());
               Urbp.setText(post.getRestbp().toString(),TextView.BufferType.EDITABLE);
                Umx.setText(post.getMaxheartrate().toString(),TextView.BufferType.EDITABLE);
                Ubs.check(post.getDiabetes().equals(1) ? R.id.radioYes : R.id.radioNo);
                Uex.check(post.getExercise().equals(1) ? R.id.radioexYes : R.id.radioexNo);
                Ucp.setSelection(post.getChestpain());
                Urew.setSelection(post.getRestelectro());
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
       View.OnClickListener listnr=new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                if(v == Usubmit){
                    final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                    process();
                }

            }
        };
        Usubmit.setOnClickListener(listnr);
        return v;
    }
    public void process()
    {
       int age1=Integer.parseInt(String.valueOf(Uage.getText()));
        //Log.d("BLB", String.valueOf(Uage.getText()));
        int cp=Ucp.getSelectedItemPosition();
        int rbp=Integer.parseInt(String.valueOf(Urbp.getText()));
        int bs;
        String Ccp=radioUbs.getText().toString().trim();
        if(Ccp=="Yes")
            bs=1;
        else
            bs=0;
        int rew;
        int rewp=Urew.getSelectedItemPosition();
            if(rewp==0)
                rew=0;
            else
                rew=rewp-1;
        int max=Integer.parseInt(String.valueOf(Umx.getText()));
        int ex;
        String Cx=radioEx.getText().toString().trim();
        if(Cx.equals("Yes"))
            ex=1;
        else
            ex=0;
        post.setHeartdisease(checkKnn());
        post.setAge(age1);
        post.setChestpain(cp);
        post.setRestbp(rbp);
        post.setDiabetes(bs);
        post.setRestelectro(rew);
        post.setMaxheartrate(max);
        post.setExercise(ex);
        post.setFcm(FirebaseInstanceId.getInstance().getToken());
        ref.setValue(post);
        viewPager.setCurrentItem(0);
    }
    public int checkKnn()
    {
        int n=200;
        double q1,q2,q3,q4,q5,q6,q7,temp;

        String In[] = new String[n];
        int d[] = new int[n];
        int c[] = new int[n];
        int r[] = new int[n];

        double x1[] ={28,29,29,31,32,32,32,33,34,34,34,35,35,35,36,36,36,36,36,37,37,37,37,37,38,38,38,38,38,38,39,39,39,39,39,39,39,39,39,39,40,40,40,40,40,41,41,41,41,41,41,41,42,42,42,42,42,42,43,43,43,43,43,44,44,44,44,44,44,45,45,45,45,45,46,46,46,46,46,46,46,46,46,46,46,47,47,47,47,47,47,48,48,48,48,48,48,48,48,48,48,48,49,49,49,49,49,49,49,49,49,50,50,50,50,50,50,50,50,50,51,51,51,51,52,52,52,52,52,52,52,52,52,52,52,52,52,53,53,53,53,53,53,53,53,53,54,54,54,54,54,54,54,54,54,54,54,54,54,54,54,54,54,55,55,55,55,55,55,55,55,55,55,55,55,56,56,56,56,56,56,56,56,57,57,57,58,58,58,58,58,58,58,58,59,59,59,59,59,59};//{28,29,31,32,34,37,39,42,46,49,56,59,53,51,62,63,63,66,44,44};
        double x2[] ={2,2,2,4,2,4,2,3,2,1,2,2,2,2,3,3,3,2,2,4,2,3,4,4,4,4,4,3,2,4,2,3,2,2,2,2,4,4,4,3,3,2,3,3,2,2,4,4,4,4,2,2,3,2,2,4,3,2,4,4,2,4,1,4,2,2,4,2,4,2,4,3,4,4,4,4,1,3,4,2,4,4,4,4,4,2,4,4,4,3,1,4,4,4,3,2,4,4,4,4,2,2,4,4,3,4,2,4,4,3,4,4,4,4,2,4,4,2,4,2,3,2,2,4,3,2,4,2,4,4,4,4,4,2,4,4,4,2,4,3,2,4,4,3,4,4,4,4,4,4,3,2,2,3,2,2,1,2,3,4,4,4,4,2,2,4,2,1,3,4,3,2,4,4,4,4,4,3,3,4,4,4,2,2,2,4,2,3,2,3,4,2,4,3,4,4,3,2,4,3};//{2,2,4,4,2,4,2,2,4,4,4,4,4,2,2,4,4,4,2,2};
        double x3[]={130,120,140,120,110,118,125,120,98,140,150,120,110,150,130,112,150,120,120,140,130,130,130,120,110,110,120,145,140,92,120,160,190,130,120,120,110,110,130,120,140,130,130,130,140,125,130,112,110,120,120,120,160,120,120,140,120,150,140,120,142,150,120,135,150,120,150,130,130,140,140,135,120,130,118,120,140,120,130,140,110,180,110,110,120,160,160,140,150,140,110,106,120,160,110,140,160,160,160,122,100,130,140,140,115,150,100,128,130,140,130,140,140,145,140,130,150,170,140,120,135,125,130,130,140,140,120,160,160,130,160,140,140,120,130,170,112,140,130,145,120,124,120,120,180,140,200,140,130,125,150,120,160,120,110,160,120,120,120,125,140,130,150,160,140,140,120,140,120,140,110,145,120,145,140,170,155,130,130,150,120,150,130,140,140,150,130,140,130,130,135,136,130,160,140,130,180,140,140,130};//{130,120,120,118,98,140,120,120,180,150,155,140,120,125,140,150,170,140,150,120};
        double x4[]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,1,0,0,0,0,0,0,0,0,0,1,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0};//{0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,1,0,0,0};
        double x5[]={2,0,0,0,0,0,0,0,0,0,1,2,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,1,1,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,1,1,0,0,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,0,1,0,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,1,0,0,0,0,1,2,0,0,0,0,0,0,1,0,0,0,1,0,0,0,0,1,0,0,0,0,1,0,1,0,1,2,0,0,0,0,0};//{2,0,0,0,0,0,1,0,1,0,0,2,0,0,0,0,0,0,0,0};
        double x6[]={185,160,170,153,184,130,155,185,150,180,168,180,140,168,178,184,172,180,160,130,98,150,158,168,166,150,170,130,150,134,160,160,106,120,145,146,150,132,140,170,188,150,138,167,172,144,130,142,170,118,170,160,146,155,150,170,152,136,135,120,138,130,155,135,150,142,170,135,100,122,144,110,140,130,124,115,175,150,112,165,150,120,140,140,125,174,158,125,98,145,150,110,115,92,138,118,103,102,99,150,100,160,130,140,175,122,174,96,120,172,170,135,140,150,170,121,140,116,125,160,150,145,150,100,170,138,150,165,82,120,94,134,124,118,110,126,96,162,148,130,132,112,116,140,120,155,142,118,91,140,122,154,130,137,142,175,137,110,150,122,105,125,134,143,150,130,137,136,134,110,160,155,140,96,128,122,150,114,128,124,140,125,100,140,145,92,110,160,150,140,100,99,140,92,119,125,100,150,140,120};//{185,160,153,130,150,130,146,155,120,122,150,119,116,145,152,115,112,94,150,142};
        double x7[]={0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,1,1,0,1,0,0,1,0,0,0,1,0,0,0,0,1,0,1,0,0,0,1,1,0,0,1,1,0,1,1,1,1,0,0,0,1,0,0,1,1,1,1,0,0,0,1,0,0,0,1,1,0,0,0,1,0,0,1,1,0,1,0,0,0,0,0,0,1,0,0,1,1,1,1,1,0,1,1,1,0,0,0,0,1,1,0,1,0,1,1,1,0,0,0,0,0,0,0,0,0,1,0,0,1,0,1,0,1,0,0,0,1,0,0,0,1,1,1,1,0,1,1,0,1,0,0,1,1,0,0,0,0,0,1,1,0,1,0,0,0,0,1};//{0,0,1,0,0,1,0,0,0,0,1,1,1,0,0,0,1,1,1,0};
        String clasi[] ={"good","good","good","bad","good","bad","good","good","good","bad","good","good","bad","good","good","good","good","good","bad","bad","good","good","good","good","bad","bad","bad","good","good","bad","good","good","good","good","good","good","bad","good","good","good","good","good","good","good","good","good","bad","good","bad","bad","good","good","good","good","good","good","good","good","bad","bad","good","bad","bad","bad","bad","good","good","good","bad","good","good","good","good","bad","bad","bad","bad","good","bad","good","bad","good","good","good","bad","good","bad","good","bad","bad","good","bad","bad","bad","good","good","bad","bad","bad","bad","good","good","good","bad","bad","bad","good","bad","bad","good","bad","good","bad","bad","good","bad","good","good","bad","good","bad","good","good","good","good","good","bad","good","bad","bad","bad","bad","bad","good","bad","bad","bad","good","good","bad","good","good","bad","good","bad","good","bad","bad","bad","bad","good","good","good","good","good","good","good","good","bad","bad","bad","bad","good","bad","good","bad","good","bad","good","good","good","good","good","bad","bad","bad","bad","good","good","bad","good","bad","good","good","bad","bad","good","good","good","bad","good","bad","bad","bad","bad","bad","good","good","good","good","bad","good","good","good","bad","bad","bad","bad","bad"
        };//{"good","good","bad","bad","good","bad","good","good","good","bad","bad","bad","bad","good","good","bad","bad","bad","bad","good"};

        q1=Integer.parseInt(String.valueOf(Uage.getText()));
        q2=Ucp.getSelectedItemPosition();
        q3=Integer.parseInt(String.valueOf(Urbp.getText()));
        String Ccp=radioUbs.getText().toString().trim();
        if(Ccp=="Yes")
            q4=1;
        else
            q4=0;
        int rewp=Urew.getSelectedItemPosition();
        if(rewp==0)
            q5=0;
        else
            q5=rewp-1;
        q6=Integer.parseInt(String.valueOf(Umx.getText()));
        String Cx=radioEx.getText().toString().trim();
        if(Cx=="Yes")
            q7=1;
        else
            q7=0;

        int k=3;

        for(int i=0;i<n;i++)
        {
            temp=(int)((x1[i]-q1)*(x1[i]-q1))+((x2[i]-q2)*(x2[i]-q2))+((x3[i]-q3)*(x3[i]-q3))+((x4[i]-q4)*(x4[i]-q4))+((x5[i]-q5)*(x5[i]-q5))+((x6[i]-q6)*(x6[i]-q6))+((x7[i]-q7)*(x7[i]-q7));
            d[i]=(int)temp;
        }
        for(int i=0;i<n;i++)
        {
            c[i]=d[i];
        }

        for(int i=0;i<n;i++)
        {
            for(int j=1;j<(n-i);j++)
            {
                if(d[j-1]>d[j])
                {
                    int t = d[j-1];
                    d[j-1] = d[j];
                    d[j] = t;
                }
            }
        }

        for(int i=0;i<n;i++)
        {
            for(int j=0;j<n;j++)
            {
                if(c[i]==d[j])
                {
                    r[i] = j+1;
                    continue;
                }
            }
        }
        int g=0,b=0;
        for(int i=0;i<n;i++)
        {
            if(r[i] < (k+1))
            {
                In[i]="yes";
                if(clasi[i].equals("good"))
                    g++;
                else
                    b++;
            }
            else
                In[i]="no";
        }
        if(g > b)
            return 0;
        else
            return 1;
    }
}
