package com.example.blb.projecta17;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Objects;

import static android.R.attr.password;
import static com.example.blb.projecta17.R.id.Password;
import static com.example.blb.projecta17.R.id.age;
import static com.example.blb.projecta17.R.id.oldpass;
import static com.example.blb.projecta17.R.id.rePassword;

/**
 * Created by BLB on 12-Mar-17.
 */

public class Change extends Fragment {
    public static final String TAG = "change";
    private EditText old;
    private EditText newpass;
    private EditText Rnew;
    private Button changeBtn;
    private FirebaseUser user;
    private ProgressDialog progressDialog;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.change, container, false);
        old=(EditText)v.findViewById(oldpass);
        newpass=(EditText)v.findViewById(Password);
        Rnew=(EditText)v.findViewById(rePassword);
        changeBtn=(Button) v.findViewById(R.id.change);
        progressDialog = new ProgressDialog(getActivity());
        View.OnClickListener listnr=new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                if(v == changeBtn){
                   final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                   process();
                }
            }
        };
        changeBtn.setOnClickListener(listnr);
        return v;
    }

    private void process() {
        String op=old.getText().toString();
        final String np=newpass.getText().toString();
        String rnp=Rnew.getText().toString();
        //Toast.makeText(getActivity(),""+op+" "+np+" "+rnp,Toast.LENGTH_SHORT).show();
        if (TextUtils.isEmpty(op)) {
            Toast.makeText(getActivity(), "Please enter old password", Toast.LENGTH_LONG).show();
            return;
        }
        if (TextUtils.isEmpty(np)) {
            Toast.makeText(getActivity(), "Please enter new password", Toast.LENGTH_LONG).show();
            return;
        }
        if (TextUtils.isEmpty(rnp)) {
            Toast.makeText(getActivity(), "Please re enter new password", Toast.LENGTH_LONG).show();
            return;
        }
        if(np.length()<6){
            Toast.makeText(getActivity(), "Password length must be atleast 6", Toast.LENGTH_LONG).show();
            return;
        }
        if (!rnp.equals(np)){
            Toast.makeText(getActivity(), "Re entered password doesn't match", Toast.LENGTH_LONG).show();
            return;
        }
        progressDialog.setMessage("Changing password Please Wait...");
        progressDialog.show();
        user = FirebaseAuth.getInstance().getCurrentUser();
        final String email = user.getEmail();
        //Toast.makeText(getActivity(), email, Toast.LENGTH_LONG).show();
        AuthCredential credential = EmailAuthProvider.getCredential(email,op);
        user.reauthenticate(credential)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            user.updatePassword(np).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        progressDialog.dismiss();
                                        old.setText("");
                                        newpass.setText("");
                                        Rnew.setText("");
                                        Toast.makeText(getActivity(), "Password updated", Toast.LENGTH_LONG).show();//Log.d(TAG, "Password updated");
                                    } else {
                                        progressDialog.dismiss();
                                        Toast.makeText(getActivity(), "Error Password updated", Toast.LENGTH_LONG).show();//Log.d(TAG, "Error password not updated");
                                    }
                                }
                            });
                        } else {
                            progressDialog.dismiss();
                            Toast.makeText(getActivity(), "Incorrect old password", Toast.LENGTH_LONG).show();//Log.d(TAG, "Error auth failed");
                        }
                    }
                });
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}
