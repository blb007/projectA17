package com.example.blb.projecta17;

import android.app.Activity;
import android.support.v4.app.Fragment;//import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import static android.R.attr.fragment;
import static android.icu.lang.UCharacter.GraphemeClusterBreak.L;
import static com.example.blb.projecta17.R.id.buttonCancel;
import static com.example.blb.projecta17.R.id.buttonLogout;

/**
 * Created by BLB on 06-Mar-17.
 */

public class Logout extends Fragment {
    public static final String TAG = "log";

    private FirebaseAuth firebaseAuth;

    private Button buttonLogout;
    private Button buttonCancel;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.logout, container, false);
        View.OnClickListener listnr=new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                if(v == buttonLogout){
                    //logging out the user
                    firebaseAuth.signOut();
                    //closing activity
                    //starting login activity
                    startActivity(new Intent(getContext(), LoginActivity.class));

                }
                if (v==buttonCancel)
                {

            Fragment fragment = getFragmentManager().findFragmentByTag(Dashboard.TAG);
            if (fragment == null) {
                fragment = new Dashboard();
            }
            getFragmentManager().beginTransaction().replace(R.id.container, fragment, Dashboard.TAG).commit();
            }
            }
        };
        buttonLogout = (Button) v.findViewById(R.id.buttonLogout);
        buttonCancel=(Button) v.findViewById(R.id.buttonCancel);
        buttonLogout.setOnClickListener(listnr);
        buttonCancel.setOnClickListener(listnr);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //initializing firebase authentication object
        firebaseAuth = FirebaseAuth.getInstance();
        //getting current user
        FirebaseUser user = firebaseAuth.getCurrentUser();
    }

}