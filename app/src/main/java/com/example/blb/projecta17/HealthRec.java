package com.example.blb.projecta17;

import android.graphics.Color;
import android.support.v4.app.Fragment;//import android.app.Fragment;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v4.widget.*;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;
import java.util.List;

import static android.R.attr.name;
import static android.R.id.content;
import static android.os.Build.VERSION_CODES.M;
import static android.support.v7.widget.AppCompatDrawableManager.get;
import static android.widget.Toast.LENGTH_LONG;
import static com.example.blb.projecta17.R.color.myTextPrimaryColor;
import static com.example.blb.projecta17.R.id.textView;
import static com.example.blb.projecta17.R.id.uage;
import static com.example.blb.projecta17.R.id.ubmi;
import static com.example.blb.projecta17.R.id.ubp;
import static com.example.blb.projecta17.R.id.uches;
import static com.example.blb.projecta17.R.id.udia;
import static com.example.blb.projecta17.R.id.ugen;
import static com.example.blb.projecta17.R.id.uglu;
import static com.example.blb.projecta17.R.id.uhd;
import static com.example.blb.projecta17.R.id.uhr;
import static com.example.blb.projecta17.R.id.uname;
import static com.example.blb.projecta17.R.id.uphone;
import static com.example.blb.projecta17.R.id.urbp;
import static com.example.blb.projecta17.R.id.ure;

/**
 * Created by BLB on 06-Mar-17.
 */

public class HealthRec extends Fragment {
    public static final String TAG = "health";
    private TextView name;
    private TextView gender;
    TextView age;
    TextView phone;
    TextView bmi;
    TextView bp;
    TextView glu;
    TextView diabetes;
    TextView chestpain;
    TextView rbp;
    TextView re;
    TextView hr;
    TextView hd;
    //private Firebase userInfoREF, userQualitiesREF, profilePicREF;
    private ValueEventListener userInfoListener;
    private FirebaseAuth firebaseAuth;
    private FirebaseUser user;
    private FirebaseDatabase database;
    private DatabaseReference ref;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.health_rec, container, false);

        firebaseAuth= FirebaseAuth.getInstance();
        user = firebaseAuth.getCurrentUser();
        database = FirebaseDatabase.getInstance();
        ref = FirebaseDatabase.getInstance().getReference(user.getUid());
       // getting views
        name=(TextView) v.findViewById(uname);
        gender=(TextView)v.findViewById(ugen);
        age=(TextView)v.findViewById(uage);
         phone=(TextView)v.findViewById(uphone);
        bmi=(TextView)v.findViewById(ubmi);
        bp=(TextView)v.findViewById(ubp);
        glu=(TextView)v.findViewById(uglu);
        diabetes=(TextView)v.findViewById(udia);
        chestpain=(TextView)v.findViewById(uches);
        rbp=(TextView)v.findViewById(urbp);
        re=(TextView)v.findViewById(ure);
        hr=(TextView)v.findViewById(uhr);
        hd=(TextView)v.findViewById(uhd);
        return v;
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
    @Override
    public void onResume() {
        super.onResume();
        readRecord();
    }

    @Override
    public void onPause() {
       // Log.e("DEBUG", "OnPause of HomeFragment");
        super.onPause();
        if(ref!=null)
        {
            ref.removeEventListener(userInfoListener);
        }
    }
    @Override
    public void onDestroy(){
        super.onDestroy();
        //if statement due to onDestroy being called in a the UpdateImage after replacing due to Orientation Change
        if(ref!=null)
        {
            ref.removeEventListener(userInfoListener);
        }
    }
    private void readRecord() {
        ref = FirebaseDatabase.getInstance().getReference(user.getUid());
        userInfoListener =  ref.addValueEventListener(new ValueEventListener() {
            public void onDataChange(DataSnapshot dataSnapshot) {
                // You can use getValue to deserialize the data at dataSnapshot
                User post = dataSnapshot.getValue(User.class);
                name.setText(post.getName());
                gender.setText(post.getGender());
                String age1=post.getAge().toString();
                age.setText(age1);

                phone.setText(post.getPhone());

                String bmi1=post.getBmi().toString();
                bmi.setText(bmi1);

                String bp1=post.getBp().toString();
                bp.setText(bp1);

                String gluco=post.getGlucose().toString();
                glu.setText(gluco);

                String diab;
                if(post.getDiabetes()==0) {
                    diab = "No";
                    diabetes.setTextColor(Color.BLUE);
                }
                else{diab="Yes";
                    diabetes.setTextColor(Color.RED);}
                diabetes.setText(diab);

                String chest = null;
                if(post.getChestpain()==1)
                    chest="Typical Angina ";
                else if(post.getChestpain()==2)
                    chest="Atypical Angina";
                else if(post.getChestpain()==3)
                    chest="Non-anginal Pain";
                else chest="Asymptomatic";
                chestpain.setText(chest);

                String restbp=post.getRestbp().toString();
                rbp.setText(restbp);

                String relec;
                if (post.getRestelectro()==0)
                    relec="Normal";
                else if(post.getRestelectro()==1)
                    relec="ST-T wave abnormality";
                else relec="left ventricular hypertrophy\n by Estes' criteria";
                re.setText(relec);

                String mh=post.getMaxheartrate().toString();
                hr.setText(mh);

                String hdi;
                if (post.getHeartdisease()==0) {
                    hdi = "No";
                    hd.setTextColor(Color.BLUE);
                }
                else{
                    hdi="Yes";
                    hd.setTextColor(Color.RED);
                }
                hd.setText(hdi);
                //Log.d(TAG, "Refreshed token: "+ FirebaseInstanceId.getInstance().getToken());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

}